![Codeship Badge](https://codeship.com/projects/e890ccf0-4e1f-0134-0254-3e029034fefa/status?branch=master
 "Codeship Badge") [![Coverage Status](https://coveralls.io/repos/github/jasonallenphotography/SmackOverflow/badge.svg?branch=codeship)](https://coveralls.io/github/jasonallenphotography/SmackOverflow?branch=master)

# SmackOverflow
SmackOverflow is a project built by two Rails developers - [Jason Allen](https://github.com/jasonallenphotography) and [Robert Reith](https://github.com/robertreith) - with the aim of building an approximation of the venerable tech forum [StackOverflow](http://www.stackoverflow.com) that is functional, well designed and responsive for desktop and mobile layouts (something that StackOverflow is not!). The goal of this app is to further develop our Rails testing skills and familiarity with various continuous integration tools, and to serve as a playground for us to tinker with various gems on a scalable, testable/well tested application.

## Tools utilized
In our creation of SmackOverflow, we used a variety of technologies that we're comfortable with, and some that we're still getting familiar with:
* Ruby on Rails (v4.2.6)
* Bootstrap CSS Framework (v3.3.6)
* CSS3/SCSS
* Javascript/JQuery
* Devise authentication
* GitHub OmniAuth authentication
* ActiveRecord (*including polymorphic associations for commentable and votable objects*)
* PostgreSQL
* RSpec-Rails for unit testing
* FactoryGirl for testing factories
* Coveralls for Automated Testing 
* Codeship Continuous Integration/Delivery
* Heroku Deployment

## Design Choices
We chose to use Bootstrap as it is a powerful tool for quickly prototyping and beautifying functional sites that is easily customizable to fit branding goals. We modified various Bootstrap classes to emulate the look and feel of StackOverflow, but also leveraged Bootstrap's powerful grid system for mobile responsive front end designs.

We chose to use RSpec-Rails for our unit testing suite as we find its syntax to be far more readable than Rails' Test Unit, and the FactoryGirl-Rails gem allowed us to implement factories in lieu of fixtures and their cumbersome *let* statements.

For continuous integration and automated testing, our past experience with SimpleCov led us to Coveralls' automated testing and report generation, and Coveralls plays nicely with both Codeship and TravisCI. As we've primarily used TravisCI previously, we opted to implement Codeship.

## Further work on SmackOverflow?
We are hopeful that SmackOverflow will serve as a well tested, stable Rails app for us to use as a playground for UI designs, user acceptance testing, testing new gems, new Javascript front end frameworks, etc.

## Contact
Email [Bobby](mailto:robertmreith@gmail.com) or [Jason](mailto:jasoncaryallen@gmail.com)!

## More work by the developers
If you'd like to see more work by the developers, please visit [Jason](https://github.com/jasonallenphotography) and [Robert](https://github.com/robertreith) on GitHub.

[Jason C. Allen, Developer](http://www.jasoncaryallen.com)

[Robert M. Reith, Developer](http://www.robertreith.com/)
