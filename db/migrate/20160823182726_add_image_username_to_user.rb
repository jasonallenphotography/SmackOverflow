class AddImageUsernameToUser < ActiveRecord::Migration
  def change
  	add_column :users, :profile_img_url, :string
    add_column :users, :username, :string
    add_column :users, :work_title, :string
    add_column :users, :location, :string
    add_column :users, :about, :text
    add_column :users, :website, :string
    end
end
