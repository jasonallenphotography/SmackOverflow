# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(
	email: "a@b.com",
	password: "123456",
	username: Faker::Superhero.name,
	profile_img_url: Faker::Avatar.image,
	work_title: Faker::Name.title,
	location: "#{Faker::Address.city}, #{Faker::Address.state}",
	about: Faker::Lorem.paragraph(2),
	website: "www.github.com",
	created_at: Faker::Time.between(DateTime.now - 60, DateTime.now - 14),
	last_sign_in_at: Faker::Time.between(DateTime.now - 2, DateTime.now))

49.times do
User.create(
	email: Faker::Internet.email,
	password: "123456",
	username: Faker::Superhero.name,
	profile_img_url: Faker::Avatar.image,
	work_title: Faker::Name.title,
	location: "#{Faker::Address.city}, #{Faker::Address.state}",
	about: Faker::Lorem.paragraph(2),
	website: "www.github.com",
	created_at: Faker::Time.between(DateTime.now - 60, DateTime.now - 14),
	last_sign_in_at: Faker::Time.between(DateTime.now - 2, DateTime.now))

end

20.times do
	Tag.create(
		name: Faker::Hacker.noun)
end

50.times do
	Question.create(
		user_id: rand(1...50),
		title: Faker::Hipster.words(4).join(" ").capitalize,
		body: Faker::Lorem.paragraphs(3).join(" ").capitalize,
		created_at: Faker::Time.between(DateTime.now - 14, DateTime.now - 2),
		updated_at: Faker::Time.between(DateTime.now - 2, DateTime.now))
end

100.times do
	QuestionTag.create(
	question_id: rand(1...50),
	tag_id: rand(1..20))
end	

100.times do
	Answer.create(
	user_id: rand(1...50),
	question_id: rand(1...50),
	body: Faker::Lorem.paragraphs(1).join(" ").capitalize,
	created_at: Faker::Time.between(DateTime.now - 14, DateTime.now - 2),
	updated_at: Faker::Time.between(DateTime.now - 2, DateTime.now))
end	

Question.all.each do |question|
	10.times do
		Vote.create(
			value: [1,-1].sample,
			user_id: rand(1...50),
			voteable_id: question.id,
			voteable_type: Question)
	end
end