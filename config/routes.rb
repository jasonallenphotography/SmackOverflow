Rails.application.routes.draw do

  devise_for :users, :controllers => { omniauth_callbacks: "callbacks",
                                       registrations: 'registrations' }

  resources :questions do
    resources :answers
    resources :question_tags, only: [:destroy]
  end
  resources :users, only: [:show]

  resources :tags, only: [:show]


  post '/questions/:id/comments', to: 'comments#question_comment_create', as: 'question_comments'
  post '/answers/:id/comments', to: 'comments#answer_comment_create', as: 'answer_comments'
  
  post '/questions/:id/votes', to: 'votes#vote_submit', as: 'votes'

  get '/questions/:question_id/:voteable_type/:id/up/create', to: 'votes#up_create', as: 'vote_up_create'
  get '/questions/:question_id/:voteable_type/:id/down/create', to: 'votes#down_create', as: 'vote_down_create'

  get '/questions/:question_id/votes/:id/update', to: 'votes#update', as: 'vote_update'
  get '/questions/:question_id/votes/:id/destroy', to: 'votes#vote_destroy', as: 'vote_destroy'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'questions#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
