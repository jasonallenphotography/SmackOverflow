require 'rails_helper'

RSpec.describe Tag, type: :model do

  context "When a tag is created" do
    let(:question1) { FactoryGirl.create :question }
    let(:question2) { FactoryGirl.create :question }
    let(:tag) { FactoryGirl.create :tag }
    
    it "belongs to a question" do
      question1.tags << tag
      expect(question1.tags.count).to be(1)
    end

    it "has a questions count of one" do
      tag.questions << question1
      expect(tag.count).to be(1)
    end

    it "has a questions count of two" do
      tag.questions << question1
      tag.questions << question2
      expect(tag.count).to be(2)
    end
  end

end
