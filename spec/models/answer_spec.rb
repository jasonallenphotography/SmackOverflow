require 'rails_helper'

RSpec.describe Answer, type: :model do
  describe "Answer" do
    let(:question)   { FactoryGirl.create :question }
    let(:answer)     { FactoryGirl.create :answer }
    let(:upvote1)    { FactoryGirl.create :answer_upvote}
    let(:upvote2)    { FactoryGirl.create :answer_upvote}
    let(:downvote1)  { FactoryGirl.create :answer_downvote}
    let(:downvote2)  { FactoryGirl.create :answer_downvote}
    let(:comment1)   { FactoryGirl.create :answer_comment}
    let(:comment2)   { FactoryGirl.create :answer_comment}


    
    context "When a answer is created" do
      it "belongs to a user" do
        expect(answer.user_id).to be_truthy
      end
       it "belongs to a question" do
        expect(answer.question_id).to be_truthy
      end

      it "has no votes" do
        expect(answer.votes.count).to eq(0)
      end

      it "has no comments" do
        expect(answer.comments.count).to eq(0)
      end
    end

    context "self.authored_by?" do
      let(:user) { FactoryGirl.create :user }
      let(:user2) { FactoryGirl.create :user }
      
      it "returns true when question.user is the author" do
        user.answers << answer
        expect(answer.authored_by?(user)).to be(true)
      end

      it "returns false when question.user is NOT the author" do
        user.answers << answer
        expect(answer.authored_by?(user2)).to be(false)
      end
    end

    context "When a answer is upvoted" do
      it "has a vote" do
        answer.votes << upvote1
        expect(answer.votes.count).to eq(1)
      end

      it "has many votes" do
        answer.votes << upvote1
        answer.votes << upvote2
        expect(answer.votes.count).to eq(2)
      end
    end

    context "When a answer's points are calculated" do
      it "has a score of one when the sum of vote values is one" do
        answer.votes << upvote1
        expect(answer.points).to eq(1)
      end

      it "has a score of zero when the sum of vote values is zero" do
        answer.votes << upvote1
        answer.votes << upvote2
        answer.votes << downvote1
        answer.votes << downvote2
        expect(answer.points).to eq(0)
      end

      it "has a score of negative one when the sum of vote values is negative one" do
        answer.votes << downvote1
        expect(answer.points).to eq(-1)
      end
    end

    context "When an answer is commented" do
      it "has a comment" do
        answer.comments << comment1
        expect(answer.comments.count).to eq(1)
      end

      it "has many comments" do
        answer.comments << comment1
        answer.comments << comment2
        expect(answer.comments.count).to eq(2)
      end
    end

  end
end
