require 'rails_helper'

RSpec.describe Vote, type: :model do
  describe "Question Upvote" do
    let(:question) { FactoryGirl.create :question }
    let(:vote)     { FactoryGirl.create :question_upvote}    


    
    context "When a question upvote is created" do
      it "belongs to a user" do
        expect(vote.user_id).to be_truthy
      end

      it "belongs to a question" do
        expect(vote.voteable_type).to eq("Question")
      end

      it "has a value of one" do
        expect(vote.value).to eq(1)
      end
    end
  end

  describe "Question Downvote" do
    let(:question) { FactoryGirl.create :question }
    let(:vote)     { FactoryGirl.create :question_downvote}    


    
    context "When a question downvote is created" do
      it "belongs to a user" do
        expect(vote.user_id).to be_truthy
      end

      it "belongs to a question" do
        expect(vote.voteable_type).to eq("Question")
      end

      it "has a value of negative one" do
        expect(vote.value).to eq(-1)
      end
    end
  end

  describe "Answer Upvote" do
    let(:answer) { FactoryGirl.create :answer }
    let(:vote)   { FactoryGirl.create :answer_upvote}    


    
    context "When an answer upvote is created" do
      it "belongs to a user" do
        expect(vote.user_id).to be_truthy
      end

      it "belongs to an answer" do
        expect(vote.voteable_type).to eq("Answer")
      end

      it "has a value of one" do
        expect(vote.value).to eq(1)
      end
    end
  end

  describe "Answer Downvote" do
    let(:answer) { FactoryGirl.create :answer }
    let(:vote)   { FactoryGirl.create :answer_downvote}    


    
    context "When an answer downvote is created" do
      it "belongs to a user" do
        expect(vote.user_id).to be_truthy
      end

      it "belongs to an answer" do
        expect(vote.voteable_type).to eq("Answer")
      end

      it "has a value of negative one" do
        expect(vote.value).to eq(-1)
      end
    end
  end

  describe "Comment Upvote" do
    let(:comment) { FactoryGirl.create :comment }
    let(:vote)    { FactoryGirl.create :comment_upvote}    


    
    context "When a comment upvote is created" do
      it "belongs to a user" do
        expect(vote.user_id).to be_truthy
      end

      it "belongs to a comment" do
        expect(vote.voteable_type).to eq("Comment")
      end

      it "has a value of one" do
        expect(vote.value).to eq(1)
      end
    end
  end

  describe "Comment Downvote" do
    let(:comment) { FactoryGirl.create :comment }
    let(:vote)    { FactoryGirl.create :comment_downvote}    


    
    context "When a comment downvote is created" do
      it "belongs to a user" do
        expect(vote.user_id).to be_truthy
      end

      it "belongs to a comment" do
        expect(vote.voteable_type).to eq("Comment")
      end

      it "has a value of negative one" do
        expect(vote.value).to eq(-1)
      end
    end
  end
end
