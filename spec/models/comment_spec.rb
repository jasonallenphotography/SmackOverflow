require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe "Question Comment" do
    let(:comment)    { FactoryGirl.create :question_comment}
    let(:question)   { FactoryGirl.create :question }
    let(:upvote1)    { FactoryGirl.create :comment_upvote}
    let(:upvote2)    { FactoryGirl.create :comment_upvote}
    let(:downvote1)  { FactoryGirl.create :comment_downvote}
    let(:downvote2)  { FactoryGirl.create :comment_downvote}


    
    context "When a question comment is created" do
      it "belongs to a user" do
        expect(comment.user_id).to be_truthy
      end
       it "belongs to a question" do
        expect(comment.commentable_type).to eq("Question")
      end

      it "has no votes" do
        expect(comment.votes.count).to eq(0)
      end
    end

    context "When a comment is upvoted" do
      it "has a vote" do
        comment.votes << upvote1
        expect(comment.votes.count).to eq(1)
      end

      it "has many votes" do
        comment.votes << upvote1
        comment.votes << upvote2
        expect(comment.votes.count).to eq(2)
      end
    end

    context "When a comment's points are calculated" do
      it "has a score of one when the sum of vote values is one" do
        comment.votes << upvote1
        expect(comment.points).to eq(1)
      end

      it "has a score of zero when the sum of vote values is zero" do
        comment.votes << upvote1
        comment.votes << upvote2
        comment.votes << downvote1
        comment.votes << downvote2
        expect(comment.points).to eq(0)
      end

      it "has a score of negative one when the sum of vote values is negative one" do
        comment.votes << downvote1
        expect(comment.points).to eq(-1)
      end
    end
  end
    describe "Answer Comment" do
    let(:comment)    { FactoryGirl.create :answer_comment}
    let(:answer)     { FactoryGirl.create :answer }
    let(:upvote1)    { FactoryGirl.create :comment_upvote}
    let(:upvote2)    { FactoryGirl.create :comment_upvote}
    let(:downvote1)  { FactoryGirl.create :comment_downvote}
    let(:downvote2)  { FactoryGirl.create :comment_downvote}


    
    context "When a question comment is created" do
      it "belongs to a user" do
        expect(comment.user_id).to be_truthy
      end
       it "belongs to an answer" do
        expect(comment.commentable_type).to eq("Answer")
      end

      it "has no votes" do
        expect(comment.votes.count).to eq(0)
      end
    end

    context "When a comment is upvoted" do
      it "has a vote" do
        comment.votes << upvote1
        expect(comment.votes.count).to eq(1)
      end

      it "has many votes" do
        comment.votes << upvote1
        comment.votes << upvote2
        expect(comment.votes.count).to eq(2)
      end
    end

    context "When a comment's points are calculated" do
      it "has a score of one when the sum of vote values is one" do
        comment.votes << upvote1
        expect(comment.points).to eq(1)
      end

      it "has a score of zero when the sum of vote values is zero" do
        comment.votes << upvote1
        comment.votes << upvote2
        comment.votes << downvote1
        comment.votes << downvote2
        expect(comment.points).to eq(0)
      end

      it "has a score of negative one when the sum of vote values is negative one" do
        comment.votes << downvote1
        expect(comment.points).to eq(-1)
      end
    end
  end
end
