	require 'rails_helper'

RSpec.describe User, type: :model do
		let(:user)   { FactoryGirl.create :user }

		context "When a user is created" do
			it "has a password or was OAuth'd via Github" do
				expect(user.provider || user.password).to be_truthy
			end

			it "has no questions" do
				expect(user.questions.count).to eq(0)
			end

			it "has no answers" do
				expect(user.answers.count).to eq(0)
			end

			it "has no votes" do
				expect(user.votes.count).to eq(0)
			end

			it "has no comments" do
				expect(user.comments.count).to eq(0)
			end
		end

		context "When a user posts questions, answers, and comments" do
			let(:question1)  	 { FactoryGirl.create :question }
			let(:question2)  	 { FactoryGirl.create :question }
			let(:answer1)    	 { FactoryGirl.create :answer }
			let(:answer2)    	 { FactoryGirl.create :answer }
			let(:comment1)   	 { FactoryGirl.create :question_comment}
			let(:comment2)   	 { FactoryGirl.create :question_comment}

			let(:q_upvote1)    { FactoryGirl.create :question_upvote}
			let(:q_upvote2)    { FactoryGirl.create :question_upvote}
			let(:q_downvote1)  { FactoryGirl.create :question_downvote}

			let(:a_upvote1)    { FactoryGirl.create :answer_upvote}
			let(:a_upvote2)    { FactoryGirl.create :answer_upvote}
			let(:a_downvote1)  { FactoryGirl.create :answer_downvote}

			let(:c_upvote1)    { FactoryGirl.create :comment_upvote}
			let(:c_upvote2)    { FactoryGirl.create :comment_upvote}
			let(:c_downvote1)  { FactoryGirl.create :comment_downvote}
			
			it "has a question that belongs to it" do
				user.questions << question1
				expect(question1.user_id).to eq(user.id)
			end

			it "has many question that belongs to it" do
				user.questions << question1
				user.questions << question2
				expect(user.questions.count).to eq(2)
			end

			it "has a score correctly reported by self.question_score_total" do
				user.questions << question1
				question1.votes << q_upvote1
				question1.votes << q_upvote2
				question1.votes << q_downvote1
				expect(user.question_score_total).to eq(1)
			end

			it "has an answer that belongs to it" do
				user.answers << answer1
				expect(answer1.user_id).to eq(user.id)
			end

			it "has many question that belongs to it" do
				user.answers << answer1
				user.answers << answer2
				expect(user.answers.count).to eq(2)
			end

			it "has a score correctly reported by self.answer_score_total" do
				user.answers << answer1
				answer1.votes << a_upvote1
				answer1.votes << a_upvote2
				answer1.votes << a_downvote1
				expect(user.answer_score_total).to eq(1)
			end

			it "has a comment that belongs to it" do
				user.comments << comment1
				expect(comment1.user_id).to eq(user.id)
			end

			it "has many comment that belongs to it" do
				user.comments << comment1
				user.comments << comment2
				expect(user.comments.count).to eq(2)
			end

			it "has a score correctly reported by self.comment_score_total" do
				user.comments << comment1
				comment1.votes << q_upvote1
				comment1.votes << q_upvote2
				comment1.votes << q_downvote1
				expect(user.comment_score_total).to eq(1)
			end

			it "has a score correctly reported by self.reputation" do
				user.questions << question1
				question1.votes << q_upvote1
				question1.votes << q_upvote2
				user.answers << answer1
				answer1.votes << a_upvote1
				answer1.votes << a_upvote2
				user.comments << comment1
				comment1.votes << c_upvote1
				comment1.votes << c_upvote2
				expect(user.reputation).to eq(6)
			end
		end

	context "When a user has cast a vote on a record self.vote_cast_id" do
		let(:question)   { FactoryGirl.create :question }
		let(:answer)  	 { FactoryGirl.create :answer }
		let(:comment)  	 { FactoryGirl.create :comment }
		let(:q_upvote)    { FactoryGirl.create :question_upvote}
		let(:a_upvote)    { FactoryGirl.create :answer_upvote}
		let(:c_upvote)    { FactoryGirl.create :comment_upvote}


		it "returns the ID of the vote the user cast on a question" do
			question.user_id = user.id
			q_upvote.user_id = user.id
			question.votes << q_upvote
			expect(user.vote_cast_id(question)).to eq(q_upvote.id)
		end

		it "returns the ID of the vote the user cast on an answer" do
			answer.user_id = user.id
			a_upvote.user_id = user.id
			answer.votes << a_upvote
			expect(user.vote_cast_id(answer)).to eq(a_upvote.id)
		end

		it "returns the ID of the vote the user cast on a comment" do
			comment.user_id = user.id
			c_upvote.user_id = user.id
			comment.votes << c_upvote
			expect(user.vote_cast_id(comment)).to eq(c_upvote.id)
		end
	end	


	context "When a user has not cast an upvote on a record" do
		let(:question)   { FactoryGirl.create :question }
		let(:answer)  	 { FactoryGirl.create :answer }
		let(:comment)  	 { FactoryGirl.create :comment }
		let(:q_downvote)    { FactoryGirl.create :question_downvote}
		let(:a_downvote)    { FactoryGirl.create :answer_downvote}
		let(:c_downvote)    { FactoryGirl.create :comment_downvote}

		it "returns the ID of the vote the user cast on a question" do
			question.user_id = user.id
			q_downvote.user_id = user.id
			question.votes << q_downvote
			expect(user.has_upVoted?(question)).to be(false)
		end

		it "returns the ID of the vote the user cast on an answer" do
			answer.user_id = user.id
			a_downvote.user_id = user.id
			answer.votes << a_downvote
			expect(user.has_upVoted?(answer)).to be(false)
		end

		it "returns the ID of the vote the user cast on a comment" do
			comment.user_id = user.id
			c_downvote.user_id = user.id
			comment.votes << c_downvote
			expect(user.has_upVoted?(comment)).to be(false)
		end
	end	

	context "When a user has not cast a downvote on a record" do
		let(:question)   { FactoryGirl.create :question }
		let(:answer)  	 { FactoryGirl.create :answer }
		let(:comment)  	 { FactoryGirl.create :comment }
		let(:q_upvote)    { FactoryGirl.create :question_upvote}
		let(:a_upvote)    { FactoryGirl.create :answer_upvote}
		let(:c_upvote)    { FactoryGirl.create :comment_upvote}

		it "returns the ID of the vote the user cast on a question" do
			question.user_id = user.id
			q_upvote.user_id = user.id
			question.votes << q_upvote
			expect(user.has_downVoted?(question)).to be(false)
		end

		it "returns the ID of the vote the user cast on an answer" do
			answer.user_id = user.id
			a_upvote.user_id = user.id
			answer.votes << a_upvote
			expect(user.has_downVoted?(answer)).to be(false)
		end

		it "returns the ID of the vote the user cast on a comment" do
			comment.user_id = user.id
			c_upvote.user_id = user.id
			comment.votes << c_upvote
			expect(user.has_downVoted?(comment)).to be(false)
		end
	end	

end
