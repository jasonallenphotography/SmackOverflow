require 'rails_helper'

RSpec.describe Question, type: :model do
	describe "Question" do
		let(:question)   { FactoryGirl.create :question }
		let(:answer1)    { FactoryGirl.create :answer }
		let(:answer2)    { FactoryGirl.create :answer }
		let(:upvote1)    { FactoryGirl.create :question_upvote}
		let(:upvote2)    { FactoryGirl.create :question_upvote}
		let(:downvote1)  { FactoryGirl.create :question_downvote}
		let(:downvote2)  { FactoryGirl.create :question_downvote}
		let(:comment1)   { FactoryGirl.create :question_comment}
		let(:comment2)   { FactoryGirl.create :question_comment}


		
		context "When a question is created" do
			it "belongs to a user" do
				expect(question.user_id).to be_truthy
			end

			it "has no answers" do
				expect(question.answers.count).to eq(0)
			end

			it "has no votes" do
				expect(question.votes.count).to eq(0)
			end

			it "has no comments" do
				expect(question.comments.count).to eq(0)
			end
		end

		context "self.formatted_date" do
			let(:question) { FactoryGirl.create :question }
			
			it "returns the time in the correct format" do
				expect (question.formatted_date).match /%b %-m '%y/
			end 
		end

		context "self.authored_by?" do
			let(:user) { FactoryGirl.create :user }
			let(:user2) { FactoryGirl.create :user }
			
			it "returns true when question.user is the author" do
				user.questions << question
				expect(question.authored_by?(user)).to be(true)
			end

			it "returns false when question.user is NOT the author" do
				user.questions << question
				expect(question.authored_by?(user2)).to be(false)
			end
		end

		context "When a question is answered" do
			it "has an answer" do
				question.answers << answer1
				expect(question.answers.count).to eq(1)
			end

			it "has multiple answers" do
				question.answers << answer1
				question.answers << answer2
				expect(question.answers.count).to eq(2)
			end
		end

		context "When a question is upvoted" do
			it "has a vote" do
				question.votes << upvote1
				expect(question.votes.count).to eq(1)
			end

			it "has many votes" do
				question.votes << upvote1
				question.votes << upvote2
				expect(question.votes.count).to eq(2)
			end
		end

		context "When a question's points are calculated" do
			it "has a score of one when the sum of vote values is one" do
				question.votes << upvote1
				expect(question.points).to eq(1)
			end

			it "has a score of zero when the sum of vote values is zero" do
				question.votes << upvote1
				question.votes << upvote2
				question.votes << downvote1
				question.votes << downvote2
				expect(question.points).to eq(0)
			end

			it "has a score of negative one when the sum of vote values is negative one" do
				question.votes << downvote1
				expect(question.points).to eq(-1)
			end
		end

		context "When a question is commented" do
			it "has a comment" do
				question.comments << comment1
				expect(question.comments.count).to eq(1)
			end

			it "has many comments" do
				question.comments << comment1
				question.comments << comment2
				expect(question.comments.count).to eq(2)
			end
		end

	end
end
