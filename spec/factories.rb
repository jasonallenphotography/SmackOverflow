FactoryGirl.define do 

  factory :user do
    sequence(:email) 			  { |n| "email#{n}@email.com"}
    sequence(:password)   	{ |n| "password#{n}"}
    sequence(:username)   	{ |n| "username#{n}"}
    sequence(:work_title)   { |n| "work_title#{n}"}
    sequence(:location)   	{ |n| "location#{n}"}
    sequence(:website) 	  	{ |n| "website#{n}"}
    sequence(:about) 		   	{ |n| "about#{n}"}
  end

  # factory :github_user do
  #   sequence(:email) 		   	{ |n| "email#{n}@email.com"}
  #   sequence(:provider) 	  { |n| "provider#{n}"}
  #   sequence(:uid)			  	{ |n| "uid#{n}"}
  #   sequence(:username) 	  { |n| "username#{n}"}
  #   sequence(:work_title)   { |n| "work_title#{n}"}
  #   sequence(:location)   	{ |n| "location#{n}"}
  #   sequence(:website) 		  { |n| "website#{n}"}
  #   sequence(:about) 		   	{ |n| "about#{n}"}
  # end

  factory :question do
    sequence(:title)        { |n| "title#{n}"}
    sequence(:body)         { |n| "body#{n}"}
    user
  end

  factory :answer do
    sequence(:body)         { |n| "body#{n}"}
    question
    user
  end

  factory :tag do
    sequence(:name)         { |n| "Tag #{n}" }
  end

  factory :comment do
    body "Body text"
    user

    trait :question_comment do
      commentable_type "Question"
      association :commentable_id, factory: :question
    end

    trait :answer_comment do
      commentable_type "Answer"
      association :commentable_id, factory: :answer
    end

    factory :question_comment, traits: [:question_comment]
    factory :answer_comment,   traits: [:answer_comment]

  end

  factory :vote do
    user

    trait :question_vote do
      voteable_type "Question"
      association :voteable_id, factory: :question
    end
    
    trait :answer_vote do
      voteable_type "Answer"
      association :voteable_id, factory: :answer
    end
    
    trait :comment_vote do
      voteable_type "Comment"
      association :voteable_id, factory: :comment
    end

    trait :up do
      value "1"
    end
    
    trait :down do
      value "-1"
    end

    factory :question_upvote,   traits: [:question_vote, :up]
    factory :question_downvote, traits: [:question_vote, :down]
    factory :answer_upvote,     traits: [:answer_vote, :up]
    factory :answer_downvote,   traits: [:answer_vote, :down]
    factory :comment_upvote,    traits: [:comment_vote, :up]
    factory :comment_downvote,  traits: [:comment_vote, :down]
  end


end