require 'rails_helper'


RSpec.describe AnswersController, type: :controller do

  include Devise::Test::ControllerHelpers
    before(:each) do
      @user = FactoryGirl.create :user
      sign_in @user
  end
  
  describe "GET #new" do
    let(:question) { FactoryGirl.create :question }

    it "returns http success" do
      get :new, { question_id: question.id }
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST #create" do
    let(:question)      { FactoryGirl.create :question }
    let(:answer_params) { FactoryGirl.attributes_for(:answer) }

    it "redirects to questions#show that the answer belongs to" do
      answer_params[:question_id] = question.id

      post :create, {answer: answer_params, question_id: question.id }
      expect(response).to redirect_to question_path(question)
    end
  end
=begin
  describe "GET #edit" do
    # let(:question) { FactoryGirl.create :question }
    let(:answer)   { FactoryGirl.create :answer }

    it "renders the answer#edit page" do
      # answer[:question_id] = question.id
      answer[:user_id] = @user.id
      # binding.pry
      # get :edit_question_answer, {answer: answer}
      get :edit, {answer: answer, question: answer.question}
      # get "/questions/#{answer.question_id}/answers/#{answer.id}/edit"
      expect(response).to render_template('edit')
    end
  end
=end

  # describe "GET #update" do
  #   it "returns http success" do
  #     get :update
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "GET #destroy" do
  #   it "returns http success" do
  #     get :destroy
  #     expect(response).to have_http_status(:success)
  #   end
  # end

end
