class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :questions
  has_many :answers
  has_many :comments
  has_many :votes

  validates :username, :email, presence: true
  validates :username, :email, uniqueness: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  def reputation
    rep = 0
    rep += question_score_total
    rep += answer_score_total
    rep += comment_score_total
    rep
  end

  def question_score_total
    score = 0
    self.questions.each do |question|
      score += question.points
    end
    score
  end

  def answer_score_total
    score = 0
    self.answers.each do |answer|
      score += answer.points
    end
    score
  end
  
  def comment_score_total
    score = 0
    self.comments.each do |comment|
      score += comment.points
    end
    score
  end

  def self.from_omniauth(auth)  
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
    end
  end

  def vote_cast_id(record)
    voteable_type = record.class
    voteable_id = record.id 
    vote = Vote.find_by(voteable_type: voteable_type, voteable_id: voteable_id, user: self )
    vote.id
  end

  def has_upVoted?(record)
    voteable_type = record.class
    voteable_id = record.id 

    return true if Vote.find_by(voteable_type: voteable_type, voteable_id: voteable_id, value: 1, user: self )
    false
  end

  def has_downVoted?(record)
    voteable_type = record.class
    voteable_id = record.id 

    return true if Vote.find_by(voteable_type: voteable_type, voteable_id: voteable_id, value: -1, user: self )
    false
  end
  
end
