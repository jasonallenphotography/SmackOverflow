class Question < ActiveRecord::Base
  belongs_to :user
  has_many :answers
  has_many :comments, as: :commentable
  has_many :votes, as: :voteable
  has_many :question_tags
  has_many :tags, through: :question_tags

  validates :title, :body, :user_id, presence: true

  def formatted_date
    self.created_at.strftime("%b %-m '%y")
  end

  def points
    votes.sum(:value)
  end

  def authored_by?(user)
    return true if self.user == user
    false
  end

end
