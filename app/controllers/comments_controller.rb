class CommentsController < ApplicationController

  def question_comment_create
    comment = Comment.new(question_comment_params)
    comment.user = current_user
    comment.commentable_type = 'Question'
    if comment.save
      if request.xhr?
        render '/comments/_comment_partial', layout: false, locals: {comment: comment}
      else
        redirect_to "/questions/#{comment.commentable_id}"
      end
    else
        @errors = ["Missing body"]
        render "/questions/#{params[:question_id]}"
    end
  end

  def answer_comment_create
    answer = Answer.find(params[:id])
    question_id = answer.question_id
    comment = Comment.new(answer_comment_params)
    comment.user = current_user
    comment.commentable_type = 'Answer'
    if comment.save
      redirect_to "/questions/#{question_id}"
    else
        @errors = ["Missing body"]
        render "/questions/#{question_id}"
    end
  end

  private
    def question_comment_params
      params.require(:comment).permit(:body, :commentable_id)
    end

    def answer_comment_params
      params.require(:comment).permit(:body, :commentable_id)
    end


end
