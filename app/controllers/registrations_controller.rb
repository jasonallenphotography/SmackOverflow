class RegistrationsController < Devise::RegistrationsController
  
  def update_resource(resource, params)
    if current_user.provider
      resource.update_without_password(account_update_params)
    else
      resource.update_with_password(account_update_params)
    end
  end


  private

  def sign_up_params
    newuser_images = ["/assets/u1.png",
								    	"/assets/u2.png",
								    	"/assets/u3.png",
								    	"/assets/u4.png",
								    	"/assets/u5.png",
								    	"/assets/u6.png"]
    params[:user][:profile_img_url] = newuser_images.sample
    params.require(:user).permit(:username,
    														 :email, 
    														 :password, 
    														 :password_confirmation,
    														 :location,
    														 :website,
    														 :profile_img_url,
    														 :work_title)
  end

  def account_update_params
    params.require(:user).permit(:username,
    														 :email, 
    														 :password, 
    														 :password_confirmation,
    														 :location,
    														 :website,
    														 :profile_img_url,
    														 :work_title,
    														 :about)
  end
end