class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :edit, :update, :destroy]
layout 'app_with_footer'
require 'will_paginate/array' 

def index
    @questions = Question.all
    @most_recent_questions = Question.paginate(:page => params[:page], :per_page => 15).order('created_at DESC')
    @most_voted_questions = Question.all.sort_by(&:points).reverse.take(25)
    @tags = Tag.all.sort_by(&:count).reverse.take(10)
  end

  def show
    @comments = @question.comments
    @tags = @question.tags
    @comment = Comment.new
    @vote = Vote.new
  end

  def new
    if user_signed_in?
      @question = Question.new
    else
      redirect_to new_user_session_path
    end
  end

  def edit
    @questiontags = QuestionTag.where(question_id: @question.id)
   if current_user.id == @question.user_id
      render "edit"
    else
      redirect_to root_path
    end
  end

  def create
    @question = Question.new(question_params)
    @question.user = current_user
    create_tags_for(@question)
    if @question.save
      redirect_to question_path(@question)
    else
      render "new"
    end
  end

  def update
    update_tags_for(@question)
    if @question.update(question_params)
      redirect_to question_path(@question)
    else
      render "edit"
    end
  end

  def destroy
    @question.destroy
    redirect_to root_path
  end

  private
    def set_question
      @question = Question.find(params[:id])
    end

    def question_params
      params.require(:question).permit(:title, :body)
    end

    def create_tags_for(question)
      tag_array = []
      tag_array = params[:question][:tags].split(',')
      tag_array.each do |tag|
        new_tag = Tag.find_or_create_by(name: tag.strip)
        question.tags << new_tag
      end
    end

    def update_tags_for(question)
      tag_array = []
      tag_array = params[:question][:tags].split(',')
      tag_array.each do |tag|
        new_tag = Tag.find_or_create_by(name: tag.strip)
        QuestionTag.create!(question_id: @question.id, tag_id: new_tag.id)
      end
    end
end
