 class QuestionTagsController < ApplicationController
  def destroy
    @question_tag = QuestionTag.find(params[:id])
    @question_tag.destroy
    @question_tags = QuestionTag.where(question_id: params[:question_id])
    if request.xhr?
      render partial: '/questions/edit_question_tags',
				layout: false,
				locals: {questiontags: @question_tags,
									question: Question.find(params[:question_id]) }
    else
      redirect_to root_path
    end
  end
end