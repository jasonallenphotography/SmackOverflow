class UsersController < ApplicationController
layout 'app_with_footer'

	def show
		@user = User.find(params[:id])
    @questions = @user.questions.order('created_at DESC').limit(20)
    @answers = @user.answers.order('created_at DESC').limit(20)
    @all_array = []

    @questions.each { |question| @all_array << question }

    @answers.each { |answer| @all_array << answer }

    @all_array.sort! { |y,x| x.created_at <=> y.created_at }

    @all = @all_array.paginate(:page => params[:page], :per_page => 25)
	end
	
end
