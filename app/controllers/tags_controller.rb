class TagsController < ApplicationController
layout 'app_with_footer'
	
  def show
    @tag = Tag.find(params[:id])
    @most_recent_questions = @tag.questions.paginate(:page => params[:page], :per_page => 10).order('created_at DESC')
    @most_voted_questions = @tag.questions.sort_by(&:points).reverse.take(25)
  end
end
