class AnswersController < ApplicationController

  def new
    set_question
    @answer = Answer.new

    if request.xhr?
      erb :'/answers/_answer_form_partial', layout: false
    else
      render 'new'
    end

  end

  def create
    set_question
    @answer = Answer.new(answer_params)
    @answer.question_id = params[:question_id]
    @answer.user_id = current_user.id
    if @answer.save
      if request.xhr?
        # erb :'/answers/_new_answer_partial', layout: false, locals: {answer: @answer}
      else
        redirect_to question_path(params[:question_id])
      end
    else
      redirect "/"
    end
  end

  def edit
    set_question
    set_answer
    render 'edit'

  end

  def update
    set_question
    set_answer
    if @answer.update_attributes(answer_params)
        redirect_to question_path(params[:question_id])
    else
      render 'edit'
    end
  end

  def destroy
    set_question
    set_answer
    @answer.destroy
    redirect_to question_path(params[:question_id])
  end


  private
    def set_question
      @question = Question.find(params[:question_id])
    end

    def set_answer
      @answer = Answer.find(params[:id])
    end

    def answer_params
      params.require(:answer).permit(:body)
    end

end




