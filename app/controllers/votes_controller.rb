class VotesController < ApplicationController

  def up_create
    vote = Vote.new(voteable_id: params[:id],
                    voteable_type: params[:voteable_type],
                    value: 1,
                    user_id: current_user.id)
    if vote.save
      if request.xhr?
        render_partial_for(vote)
      else
        redirect_to "/questions/#{params[:question_id]}"
      end
    else
        @errors = vote.errors.full_messages
        redirect_to "/questions/#{params[:question_id]}"
    end 
  end

  def down_create
    vote = Vote.new(voteable_id: params[:id],
                    voteable_type: params[:voteable_type],
                    value: -1,
                    user_id: current_user.id)
    if vote.save
      if request.xhr?
        render_partial_for(vote)
      else
          redirect_to "/questions/#{params[:question_id]}"
      end
    else
      @errors = vote.errors.full_messages
        redirect_to "/questions/#{params[:question_id]}"
    end 
  end

  def update
    vote = Vote.find(params[:id])
    if current_user.id == vote.user_id
      vote.value == 1 ? vote.update_attributes(value: -1) : vote.update_attributes(value: 1)
       
      if request.xhr?
        render_partial_for(vote)
      else
          redirect_to "/questions/#{params[:question_id]}"
      end
    else
      @errors = vote.errors.full_message
      render '/questions/show'
    end
    
  end

  def vote_destroy
    vote = Vote.find(params[:id])
    if current_user.id == vote.user_id
      if request.xhr?
          vote.destroy
          render_partial_for(vote)
      else
        redirect_to "/questions/#{params[:question_id]}"
      end
    end 
  end

  protected

    def render_partial_for(vote)
      case vote.voteable_type
        when "Answer"
          a = Answer.find(vote.voteable_id)
          render partial: '/votes/answer_vote', layout: false, locals: { answer: a }
        when "Question"
          q = Question.find(vote.voteable_id)
          render partial: '/votes/question_vote', layout: false, locals: { question: q } 
        else 
          if question_comment_vote?(vote)
            c = Comment.find(vote.voteable_id) 
            render partial: '/questions/comment', layout: false, locals: { comment: c }
          else
            c = Comment.find(vote.voteable_id) 
            a = Answer.find(c.commentable_id)
            render partial: '/questions/answer_comment', layout: false, locals: { comment: c, question: a.question }
          end
        end
      end

    def question_comment_vote?(vote)
      true if vote.voteable_type == "Comment" && vote.voteable.commentable_type == "Question"
    end

end
