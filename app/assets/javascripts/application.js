// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require_tree .

$(document).ready(function() {

	$('#questiontags').on('click', 'a', function(e){
		e.preventDefault();
		var $target = $(e.target)
		$.ajax({
			url: $target.attr('href'),
			method: $target.attr('method'),
			data: $target.serialize()
		}).done(function(response){
			$('#questiontags').html(response)
		});

	});

	$('#clear_img').on('click', function(e){
		e.preventDefault();
		var $target = $(e.target)
		$('#img_field').val('');
	});

	$('.question-response').on('click', '.question-vote', function(e){
		event.preventDefault();
		var $target = $(e.target)
		$.ajax({
			url: $target.closest('a').attr('href'),
			method: $target.attr('method'),
			data: $target.serialize()
		}).done(function(response){
			$target.closest('.question-response').html(response);
		}).fail(function(error){
			console.log("Ajax Error")
		});
	});

	$('.question-comment-response').on('click', '.comment-vote', function(e){
		event.preventDefault();
		var $target = $(e.target)
		$.ajax({
			url: $target.closest('a').attr('href'),
			method: $target.attr('method'),
			data: $target.serialize()
		}).done(function(response){
			$target.closest('.question-comment-response').html(response);
		}).fail(function(error){
			console.log("Ajax Error")
		});
	});

	$('.answer-response').on('click', '.answer-vote', function(e){
		event.preventDefault();
		var $target = $(e.target)
		$.ajax({
			url: $target.closest('a').attr('href'),
			method: $target.attr('method'),
			data: $target.serialize()
		}).done(function(response){
			$target.closest('.answer-response').html(response);
		}).fail(function(error){
			console.log("Ajax Error")
		});
	});


	$('.answer-comment-response').on('click', '.answer-comment-vote', function(e){
		event.preventDefault();
		var $target = $(e.target)
		$.ajax({
			url: $target.closest('a').attr('href'),
			method: $target.attr('method'),
			data: $target.serialize()
		}).done(function(response){
			$target.closest('.answer-comment-response').html(response);
		}).fail(function(error){
			console.log("Ajax Error")
		});
	});
});